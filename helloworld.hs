doubleMe x = x + x

doubleUs x y = x*2 + y*2

doubleSmallNumber x = if x > 100
                  then x
                  else x*2

doubleSmallNumber' x = (if x > 100
                  then x
                  else x*2) + 1

conanO'Brien = "It's a-me, Conan O'Brien!"

getIndex x y = x !! y

factorial x = product [1..x]

boomBangs xs = [if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]

removeNonUppercase st = [ c | c <- st, elem c ['A'..'Z']]

circumference r = 2 * pi * r

circumference' :: Double -> Double
circumference' r = 2 * pi * r

mult3and5 x = [x | x <- [1..x-1], mod x 3 == 0 || mod x 5 == 0]
